package ru.gavrilin.animals;

/**
 * Created by Макс on 16.10.2014.
 */
public class Dog implements Animals{

    @Override
    public String getName() {
        return "собака";
    }

    @Override
    public String getVoice() {
        return "гав гав";
    }
}
