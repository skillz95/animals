package ru.gavrilin.animals;

/**
 * Created by Макс on 16.10.2014.
 */
public class Cat implements Animals{

    @Override
    public String getName() {
        return "кошка";
    }

    @Override
    public String getVoice() {
        return "мяу мяу";
    }
}
