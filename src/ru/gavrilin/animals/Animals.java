package ru.gavrilin.animals;

/**
 * Created by Макс on 16.10.2014.
 */
public interface Animals {

    public String getName();
    public String getVoice();

}
